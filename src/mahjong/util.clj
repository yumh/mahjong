(ns mahjong.util)

(defn make-priority
  "Given a vector of elements, return a map made of that elements and
  their priority. `[:red :green :white]` becomes `{:red 0, :green 2,
  :white 2}`."
  [v]
  (into {} (map #(vector %1 %2) v (range))))

(defn permutations [s]
  (lazy-seq
   (if (seq (rest s))
     (apply concat (for [x s]
                     (map #(cons x %) (permutations (remove #{x} s)))))
     [s])))

(defn remove-first [e coll]
  (let [[n m] (split-with #(not= e %) coll)]
    (concat n (rest m))))
