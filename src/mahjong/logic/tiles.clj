(ns mahjong.logic.tiles
  (:require [mahjong.util :refer [make-priority]]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as stest]))

(require '[clojure.spec.gen.alpha :as gen])

(def suits?       #{:dragon :wind :bamboo :pinzu :manzu})
(def winds?       #{:east :west :north :south})
(def dragons?     #{:red :white :green})
(def honor?       (into #{} (concat winds? dragons?)))
(def simples?     (into #{} (range 2 9)))
(def terminal?    #{1 9})
(def numbered?    (into #{} (concat simples? terminal?)))
(def suit-honor?  #{:dragon :wind})
(def suit-simple? #{:bamboo :pinzu :manzu})

;; spec for tiles

(defn value-valid? [{:keys [suit value]}]
  (boolean
   (condp = suit
     :dragon (dragons? value)
     :wind   (winds? value)
     (numbered? value))))

(defn tile-simple? [{s :suit}]
  (suit-simple? s))

(s/def :mahjong/tile (s/and (s/keys :req-un [:mahjong/suit :mahjong/value]
                             :opt-un [:mahjong/kan])
                      value-valid?))
(s/def :mahjong/simple (s/and :mahjong/tile
                              tile-simple?))
(s/def :mahjong/kan boolean?)
(s/def :mahjong/suit suits?)
; (s/def :mahjong/value (s/or :dragon dragons?
;                      :honor  honor?
;                      :simple simples?))
(s/def :mahjong/value (into #{} (concat dragons? honor? numbered?)))

; (s/explain :mahjong/tile (manzu 1))
; (gen/generate (s/gen :mahjong/tile))

;; general function to easily create tiles

(defn dragon [t] {:suit :dragon
                  :value t})

(defn wind [t] {:suit :wind
                :value t})

(defn bamboo [n] {:suit :bamboo
                  :value n})

(defn pinzu [n] {:suit :pinzu
                 :value n})

(defn manzu [n] {:suit :manzu
                 :value n})

(defn dragon? [{s :suit}]
  (= s :dragon))

(defn wind? [{s :suit}]
  (= s :wind))

(defn bamboo? [{s :suit}]
  (= s :bamboo))

(defn pinzu? [{s :suit}]
  (= s :pinzu))

(defn manzu? [{s :suit}]
  (= s :manzu))

(defn by-kan [t]
  (assoc t :kan true))

(defn by-kan? [{k :kan}]
  k)

(defn suit [{s :suit}]
  s)

(defn value [{v :value}]
  v)

(defn clean-tile "Filter out any key on t besides of :suit and :value"
  [t]
  (select-keys t [:suit :value]))

(defn tile=?
  "True if the tiles are equal. Tiles are considered equal if they
  have the same `:suit` and `:value`"
  [& tiles]
  (every? identity
          (map (partial apply #(= (clean-tile %1) (clean-tile %2)))
               (partition 2 1 tiles tiles))))

(s/fdef tile=?
  :args (s/* :mahjong/tile)
  :ret boolean?)

(defn next-tile [t]
  (if-not (or (= (value t) 9) (dragon? t) (wind? t))
    (update t :value inc)))

(s/fdef next-tile
  :args (s/cat :t :mahjong/tile)
  :ret (s/or :tile :mahjong/tile
             :not-found nil?)
  :fn #(if-not (nil? (:ret %))
         (= (suit (:ret %)) (suit (-> % second :args :t)))
         true))
; (stest/instrument `next-tile)
; (stest/instrument `mahjong.logic.tiles)
; (next-tile (pinzu :red))

(defn prec-tile [t]
  (if-not (or (= (value t) 1) (dragon? t) (wind? t))
    (update t :value dec)))

(s/fdef prec-tile
  :args (s/cat :t :mahjong/tile)
  :ret (s/or :tile :mahjong/tile
             :not-found nil?)
  :fn #(if-not (nil? (:ret %))
         (= (suit (:ret %)) (suit (-> % second :args :t)))
         true))
; (stest/instrument `prec-tile)

;; tiles function

(defn one-set "Generate one set." []
  (concat
   (for [f [pinzu manzu bamboo]
         x (range 1 10)]
     (f x))
   (for [t [:red :white :green]]
     (dragon t))
   (for [t [:east :west :north :south]]
     (wind t))))

(s/fdef one-set
  :args (s/cat)
  :ret (s/* :mahjong/tile))
; (stest/instrument `one-set)
; (stest/abbrev-result (first (stest/check `one-set)))

(defn full-set "Generate a full set." []
  (apply concat (repeat 4 (one-set))))

; (full-set)
; (shuffle (one-set))

(defn tile<? "`true` if t1 < t2, for all t1, t2 tiles"
  [{s1 :suit, v1 :value} {s2 :suit, v2 :value}]
  (let [suits-priority   (make-priority [:manzu :pinzu :bamboo :wind :dragon])
        dragons-priority (make-priority [:white :green :red])
        winds-priority   (make-priority [:east :south :west :north])]
    (apply compare
           (cond (not= s1 s2)   [(suits-priority s1)   (suits-priority s2)]
                 (= s1 :dragon) [(dragons-priority v1) (dragons-priority v2)]
                 (= s1 :wind)   [(winds-priority v1)   (winds-priority v2)]
                 :else          [v1 v2]))))

(defn sort-tiles [s]
  (sort tile<? s))

(defn to-string [{:keys [suit value]}]
  (str suit value))
