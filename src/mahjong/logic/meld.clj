(ns mahjong.logic.meld
  (:require [mahjong.logic.tiles :as t]
            [mahjong.util :as u]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.gen.alpha :as gen]))

;; Spec + utility

(defn same-suit? [tiles]
  (apply = (map t/suit tiles)))

(defn is-consec-triple? [s]
  (let [ss (t/sort-tiles s)
        [x y z] ss]
    (and (t/tile=? (t/next-tile x) y)
         (t/tile=? (t/next-tile y) z))))

(defn quad-gen   [] (gen/fmap #(repeat 4 %) (s/gen :mahjong/tile)))
(defn triple-gen [] (gen/fmap #(repeat 3 %) (s/gen :mahjong/tile)))
(defn seq-gen    [] (gen/fmap #(take 3 (iterate t/next-tile %)) (s/gen :mahjong/simple)))

(s/def :mahjong/meld (s/or :quad   (s/with-gen (s/and (s/coll-of :mahjong/tile :count 4)
                                                      #(apply t/tile=? %))
                                               quad-gen)
                           :triple (s/with-gen (s/and (s/coll-of :mahjong/tile :count 3)
                                                      #(apply t/tile=? %))
                                               triple-gen)
                           :seq    (s/with-gen (s/and (s/coll-of :mahjong/simple :count 3)
                                                      is-consec-triple?)
                                               seq-gen)))

; (s/conform :mahjong/meld (gen/generate (s/gen :mahjong/meld)))

(defn meld "Return a new meld or a false-ish value. If not specified, is assumed that it's open."
  ([tiles] (meld true tiles))
  ([open? tiles]
   (let [c (s/conform :mahjong/meld tiles)]
     (if-not (= c :clojure.spec.alpha/invalid)
       {:tiles (second c)
        :type  (first c)
        :open  open?}))))

(s/fdef meld
  :args (s/alt :implicit-open (s/cat :tiles (s/* :mahjong/tile))
               :esplicit-open (s/cat :open? boolean? :tiles (s/coll-of :mahjong/tile))))
; (stest/instrument `meld)

(s/fdef all-melds
  :args (s/coll-of :mahjong/tile))
; (stest/instrument `all-melds)

(defn sequential-melds [tiles]
  (map meld (partition 3 tiles)))

(defn find-melds [tiles]
  (loop [[t  & r]   tiles
         [t' & r'] (rest tiles)
         till-now  []
         found     []]
    (cond
      (nil? t) found ;; we've finished

      (t/tile=? t t') ;; ok, we got a possible couple
      (let [ms (sequential-melds (concat till-now r'))]
        (if (some nil? ms)
          (recur r r' (conj till-now t) found)
          (recur r r' (conj till-now t) (conj found {:couple [t t']
                                                     :melds  ms}))))

      ;; nope....
      :else (recur r r' (conj till-now t) found))))
