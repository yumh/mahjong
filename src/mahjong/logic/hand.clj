(ns mahjong.logic.hand
  (:require [mahjong.logic.tiles :as t]
            [mahjong.util :refer [remove-first]]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as stest]))

;; TODO: add spec

(defn hand []
  {:tiles     []
   :calls     []
   :last-tile nil})

(defn last-tile [{h :last-tile}]
  h)

(defn tiles [{t :tiles}]
  t)

(defn count-tiles [hand tile]
  (count (filter (partial t/tile=? tile)
                 (tiles hand))))

(defn add-tile [hand tile]
  ;; TODO: naïve way to add a tile.
  (-> hand
      (update :tiles (comp t/sort-tiles conj) tile)
      (assoc :last-tile tile)))

(s/fdef add-tile
        :args (s/cat :hand map?
                     :tile :mahjong/tile)
        :ret   map?)
; (stest/instrument `add-tile)
; (add-tile {} (t/dragon :red))

(defn add-pon [hand tile]
  (let [c (count-tiles hand tile)]
    ))

(defn- remove-tile-with-fn
  "Update the hand with `f` then, if `:last-tile` is equals to the
  given `tile`, set it to `nil`."
  [hand tile f]
  (let [h (update hand :tiles #(f (partial t/tile=? tile) %))]
    (if (t/tile=? (last-tile h))
      (assoc h :last-tile nil)
      h)))

(defn remove-tile "Remove the first tile equal to the given. May set `:last-tile` to nil."
  [hand tile]
  (remove-tile-with-fn hand tile remove-first))

(defn remove-tiles "Remove *all* tiles equal to the given. May set `:last-tile` to nil."
  [hand tile]
  (remove-tile-with-fn hand tile remove))
