(ns mahjong.logic.tiles-test
  (:require [clojure.test :refer :all]
            [mahjong.logic.tiles :refer :all]))

(deftest tiles
  (testing "Tile equality"
    (is (every? identity
                (map #(tile=? % %)
                     (one-set)))
        "Every tile should be equal to itself.")

    (is (tile=? (dragon :red)
                (-> (dragon :red)
                    (assoc :foo 'bar)))
        "Any property other than :suit and :value should not
        interfere."))

  (testing "Set generation"
    (is (= (count (into #{} (one-set)))
           (count (one-set)))
        "One set should not contain repetition.")

    (is (= (count (one-set)) 34)
        "One set is made up of 34 tiles.")

    (is (= (count (full-set))
           136)
        "A full set should be made up of 136 tiles.")

    (is (= (into #{} (one-set))
           (into #{} (full-set)))
        "A full set should have the same tile of a set (minor repetition ofc).")))
